#!/usr/bin/env bash
echo "Initializing multi-backupper"

mc config host add bksrv $BACKUP_S3_HOST $BACKUP_S3_USER $BACKUP_S3_PASS

BACKUP_S3_BUCKET=${BACKUP_S3_BUCKET:-"$BACKUP_NAME-backup"}

cd /tmp

dt=`date '+%Y%m%d_%H%M%S'`
filename=$dt"_database_"$BACKUP_NAME
foldername=$dt"_files_"$BACKUP_NAME

mkdir $foldername

echo $filename

IGNORELIST=""

for I in $BACKUP_IGNORE_TABLES
do
  IGNORELIST="$IGNORELIST --ignore-table=$DB_DATABASE.$I"
done

mysqldump -u$DB_USERNAME -p$DB_PASSWORD -h $DB_HOST --port=$DB_PORT --set-gtid-purged=OFF --single-transaction$IGNORELIST $DB_DATABASE > $filename".sql"

tar -czvf $filename".tar.gz" $filename".sql"
rm $filename".sql"

mc cp $filename".tar.gz" "bksrv/$BACKUP_S3_BUCKET/"
rm $filename".tar.gz"


if [ -n "$BACKUP_FILES" ];
then
  for F in $BACKUP_FILES
  do
    cp -r /data/$F /tmp/$foldername/
  done

  tar -czvf $foldername".tar.gz" -C /tmp/$foldername .

  mc cp $foldername".tar.gz" "bksrv/$BACKUP_S3_BUCKET/"
  rm $foldername".tar.gz"

  rm -r /tmp/$foldername
else
  echo "no files to backup"
fi

echo "Done!"
