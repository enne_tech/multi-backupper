FROM ubuntu:bionic

RUN apt-get update && apt-get install -y mysql-client wget

RUN wget https://dl.min.io/client/mc/release/linux-amd64/mc -O /usr/local/bin/mc

RUN chmod +x /usr/local/bin/mc

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]