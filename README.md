## Example usage
```
#!/usr/bin/env bash
WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
docker run --rm -ti --env-file "$WORKDIR/.env" -v "$WORKDIR:/data" registry.gitlab.com/enne_tech/multi-backupper
```

## Variable list
- BACKUP_S3_HOST
- BACKUP_S3_USER
- BACKUP_S3_PASS
- BACKUP_NAME
- BACKUP_IGNORE_TABLES
- BACKUP_FILES
- DB_HOST
- DB_PORT
- DB_USERNAME
- DB_PASSWORD
- DB_DATABASE